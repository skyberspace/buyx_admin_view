import { getPageMode } from '@/pageMode'
import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router';
import { admin_routes } from './admin-routes';
import { restaurant_routes } from './restaurant-routes';

Vue.use(VueRouter)

let routes: Array<RouteConfig> = [];

let pageMode = getPageMode();
if (pageMode == "admin") {
  routes = admin_routes;
}
else if (pageMode == "restaurant") {
  routes = restaurant_routes;
}
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
