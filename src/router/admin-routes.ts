
import { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'


export let admin_routes: Array<RouteConfig> = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/categories',
        name: 'Categories',
        component: () => import(/* webpackChunkName: "about" */ '../views/Admin/Category/CategoryList.vue')
    },
    {
        path: '/categories/:id',
        name: 'Category Detail',
        component: () => import(/* webpackChunkName: "about" */ '../views/Admin/Category/CategoryDetail.vue'),
        props: true,

    },
    {
        path: '/users',
        name: 'Users',
        component: () => import(/* webpackChunkName: "about" */ '../views/Admin/User/Users.vue')
    },
    {
        path: '/users/:id/*',
        name: 'User Detail',
        component: () => import(/* webpackChunkName: "about" */ '../views/Admin/User/UserDetail.vue'),
        props: true
    },


    {
        path: '/products',
        name: 'Product List',
        component: () => import(/* webpackChunkName: "about" */ '../views/Admin/Product/ProductList.vue')
    },
    {
        path: '/products/:id/*',
        name: 'Product Detail',
        component: () => import(/* webpackChunkName: "about" */ '../views/Admin/Product/ProductDetail.vue'),
        props: true
    },


    {
        path: '/warehouses',
        name: 'Warehouse List',
        component: () => import(/* webpackChunkName: "about" */ '../views/Admin/Warehouse/Warehouses.vue')
    },
    {
        path: '/warehouses/:id/*',
        name: 'Warehouse Detail',
        component: () => import(/* webpackChunkName: "about" */ '../views/Admin/Warehouse/WarehouseDetail.vue'),
        props: true
    },


    {
        path: "/blog",
        name: "Blog Entries",
        component: () => import(/* webpackChunkName: "about" */ '../views/Admin/BlogEntry/BlogEntryList.vue')
    },
    {
        path: '/blog/:id/*',
        name: 'Blog Entry Detail',
        component: () => import(/* webpackChunkName: "about" */ '../views/Admin/BlogEntry/BlogEntryDetail.vue'),
        props: true
    },


    {
        path: '/orders',
        name: 'Order List',
        component: () => import(/* webpackChunkName: "about" */ '../views/Admin/Order/OrderList.vue')
    },

    {
        path: '/orders/:id/*',
        name: 'Order Detail',
        component: () => import(/* webpackChunkName: "about" */ '../views/Admin/Order/OrderDetail.vue'),
        props: true
    },


    {
        path: '/restaurants',
        name: 'Restaurant List',
        component: () => import(/* webpackChunkName: "about" */ '../views/Admin/Restaurant/Restaurants.vue')
    },
    {
        path: '/restaurants/:id',
        name: 'Restaurant Detail',
        component: () => import(/* webpackChunkName: "about" */ '../views/Admin/Restaurant/RestaurantDetail.vue'),
        props: true

    },
]