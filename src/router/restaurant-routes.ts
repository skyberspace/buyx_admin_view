
import { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'


export let restaurant_routes: Array<RouteConfig> = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },

    {
        path: '/info',
        name: 'BuyxFoodInfo',
        component: () => import(/* webpackChunkName: "about" */ '../views/Restaurant/BuyxFoodInfo/BuyxFoodInfo.vue'),

    },

    {
        path: '/orders',
        name: 'BuyxFoodOrder',
        component: () => import(/* webpackChunkName: "about" */ '../views/Restaurant/BuyxFoodOrder/BuyxFoodOrderList.vue'),

    },

    {
        path: '/orders/:id/*',
        name: 'BuyxFoodOrder Detail',
        component: () => import(/* webpackChunkName: "about" */ '../views/Restaurant/BuyxFoodOrder/BuyxFoodOrderDetail.vue'),
        props: true,

    },

    {
        path: '/products',
        name: 'BuyxFoodProduct',
        component: () => import(/* webpackChunkName: "about" */ '../views/Restaurant/BuyxFoodProduct/BuyxFoodProductList.vue'),

    },

    {
        path: '/products/:id/*',
        name: 'BuyxFoodProduct Detail',
        component: () => import(/* webpackChunkName: "about" */ '../views/Restaurant/BuyxFoodProduct/BuyxFoodProductDetail.vue'),
        props: true,
    },

]