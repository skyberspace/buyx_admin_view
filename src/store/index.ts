import { getPageMode } from '@/pageMode'
import Vue from 'vue'
import Vuex from 'vuex'
import API, { AuthToken, User } from "../api"
Vue.use(Vuex)


export interface State {
  user: User | null;
  token: AuthToken | null;
  loadingUser: boolean
}


export default new Vuex.Store<State>({
  state: {
    user: null,
    token: null,
    loadingUser: false,
  },
  getters: {
    isLoggedIn(state) {
      if (state.loadingUser) {
        return false;
      }
      if (state.user == null) {
        return false;
      }
      return true;
    },
    pageMode() {
      return getPageMode();
    },
  },
  mutations: {
    setLoading(state, payload) {
      state.loadingUser = payload;
    },
    setUserToken(state, payload) {
      console.log(payload)
      state.user = payload.user;
      state.token = payload.token;
      localStorage.setItem("buyx:token", payload.token || "");
      state.loadingUser = false;
    },
    logout(state,) {
      state.user = null;
      state.token = null;
      localStorage.setItem("buyx:token", "");
    }
  },
  actions: {
    async logout(context): Promise<void> {
      const token = localStorage.getItem("buyx:token");
      const res = await API.AuthApi.adminAuthLogoutPost({ token: token })
      if (res.data.success == true) {
        context.commit("logout");
      }
    }
  },
  modules: {
  }
})
