import axios from 'axios';
import Swal from 'sweetalert2';
import API from './api';
import RAPI from './restaurant-api';
import store from './store';

const appRootUrlDev = "http://localhost:8080";
const appRootUrl = "https://buyx.skyberspace.com";
function getBaseUrl(): string {

    if (window.location.href.indexOf("//localhost") != -1 || window.location.href.indexOf("127.0.0.1") != -1 || window.location.href.indexOf("192.168") != -1) {
        return appRootUrlDev;
    }
    return appRootUrl;

}
let axiosInstance = axios.create()

axiosInstance.interceptors.request.use((config) => {
    //console.log(config);
    let token = localStorage.getItem("buyx:token");
    // if (config.data)
    //  config.data.token = // Vue.cookie.get("userToken");
    if (token) {
        config.headers["Authorization"] = "Bearer " + token;
    }
    return config;
},
    (error) => {
        // Do something with request error
        return Promise.reject(error);
    });


axiosInstance.interceptors.response.use(
    response => {
        if (response.data == null) {
            return response;
        }
        if (response.data.redirectToLogin) {
            store.commit("Logout");

            //console.log(staticStore);
            Promise.reject(response);

            Swal.fire("Error", "To continue, please first login.", "error");

        }//http://localhost:60410/Home/CheckLogin

        if (response.data.success === false && !response.data.redirectToLogin) {
            console.log("#################### Error ###################");
            console.log("#################### Message ###################");
            console.log(response);
            console.log("################# End of message ################");
            if (response.data.errors.length > 0) {
                Swal.fire("Error", response.data.errors[0], "error");
            } else {
                Swal.fire("Hata", "An error occured.", "error");
            }

            Promise.resolve(response);
        }
        return response;
    },
    error => {
        console.log("#################### Error ###################");
        console.log("#################### Message ###################");
        console.log(error);
        console.log("################# End of message ################");

        if (error.response && error.response.data) {
            Swal.fire("Error", error.response.data.errors[0], "error");
        }
        else {
            Swal.fire("Error", "An eror occured.", "error");
        }
        return Promise.reject(error);
    }
);

API.initialize(getBaseUrl(), axiosInstance);
RAPI.initialize(getBaseUrl(), axiosInstance);

