export type VLoadable = Vue & { load: () => void }
