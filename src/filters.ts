import Vue from 'vue';
import numeral from 'numeral';

/**
 * Better DateTimeFormatOptions types
 * 
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat/DateTimeFormat for representation details
 */
export interface DateTimeFormatOptions extends Intl.DateTimeFormatOptions {
    localeMatcher?: 'best fit' | 'lookup';
    weekday?: 'long' | 'short' | 'narrow';
    era?: 'long' | 'short' | 'narrow';
    year?: 'numeric' | '2-digit';
    month?: 'numeric' | '2-digit' | 'long' | 'short' | 'narrow';
    day?: 'numeric' | '2-digit';
    hour?: 'numeric' | '2-digit';
    minute?: 'numeric' | '2-digit';
    second?: 'numeric' | '2-digit';
    timeZoneName?: 'long' | 'short';
    formatMatcher?: 'best fit' | 'basic';
    hour12?: boolean;
    /**
     * Timezone string must be one of IANA. UTC is a universally required recognizable value
     */
    timeZone?: 'UTC' | string;
    dateStyle?: 'full' | 'long' | 'medium' | 'short',
    timeStyle?: 'full' | 'long' | 'medium' | 'short',
    calendar?: 'buddhist' | 'chinese' | ' coptic' | 'ethiopia' | 'ethiopic' | 'gregory' | ' hebrew' | 'indian' | 'islamic' | 'iso8601' | ' japanese' | 'persian' | 'roc',
    dayPeriod?: 'narrow' | 'short' | 'long',
    numberingSystem?: 'arab' | 'arabext' | 'bali' | 'beng' | 'deva' | 'fullwide' | ' gujr' | 'guru' | 'hanidec' | 'khmr' | ' knda' | 'laoo' | 'latn' | 'limb' | 'mlym' | ' mong' | 'mymr' | 'orya' | 'tamldec' | ' telu' | 'thai' | 'tibt',
    hourCycle?: 'h11' | 'h12' | 'h23' | 'h24',
    /**
     * Warning! Partial support 
     */
    fractionalSecondDigits?: 0 | 1 | 2 | 3
}


Vue.filter("dateTime", function (a: string) {
    if (a != null) {
        if (a == "0001-01-01T00:00:00Z")
            return "Empty";

        return Intl.DateTimeFormat("en-GB", {
            day: "numeric",
            month: "numeric",
            year: "numeric",
            hour: "numeric",
            minute: "numeric",
            timeZoneName: "short",
            second: "numeric",
            timeZone: "Europe/London",
        }).format(new Date(a))
    }
    return "";
});


Vue.filter("format", function (s: any) {
    return numeral(s).format("0,0[.]00");
});