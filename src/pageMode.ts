export function getPageMode(): string {
    if (window.location.hostname.startsWith("192") || window.location.hostname.startsWith("127") || window.location.hostname.startsWith("restaurant.skyberspace")) {
        return "restaurant";
    }
    else {
        return "admin"
    }
}